<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url]
[![Forks][forks-shield]][forks-url]
[![Stargazers][stars-shield]][stars-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]][linkedin-url] -->



<!-- PROJECT LOGO -->
<br />
<div align="center">

  <h3 align="center">Angelica</h3>

  <p align="center">
    A companion app to help victims of domestic violence
    <!-- <br />
    <a href="https://github.com/othneildrew/Best-README-Template"><strong>Explore the docs »</strong></a> -->
    <!-- <br />
    <br />
    <a href="https://github.com/othneildrew/Best-README-Template">View Demo</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Report Bug</a>
    ·
    <a href="https://github.com/othneildrew/Best-README-Template/issues">Request Feature</a> -->
  </p>
</div>



<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#description">Description</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <!-- <li><a href="#roadmap">Roadmap</a></li> -->
    <!-- <li><a href="#contributing">Contributing</a></li> -->
    <!-- <li><a href="#license">License</a></li> -->
    <li><a href="#members">Members</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Domestic violence or KDRT in Indonesian is defined as an act of violence physically or mentally to the victim by aggression among people with kinship. The victim in the family could be anyone such as children, women, men, or the elderly. Domestic violence issues can range from minor impact like humiliation to concerning impact such as sexual abusement (Holt, 2008). In reality, it doesn't matter if it's only small things like humiliation, it still gives a bad impact to the victim and it won’t guarantee it will not escalate to the next level. In the end the victim will be left with posttraumatic stress and insecurity that could be carried for the rest of their life. 

Due to the COVID-19 pandemic resulting in some countries conducting lockdown for their citizens, there has been an increase of reports on domestic violence. For example, there has been an increase of 40% in reported domestic violence in Brazil (Bradbury-Jones, 2020). This incident happens because citizens need to battle the pandemic, which in turn isolates them alongside their abusers, increasing the frequency of experiencing domestic violence  (Kofman, 2020). A big concern is when the victims become reluctant or unable to show evidence when reporting the acts of violence that have befallen them.

Our proposed solution, as to be our project, is a device that would be capable of discreetly collecting evidence of domestic violence by way of audio and video recording. In order to minimize misuse of the device and save storage space, it will only start recording and storing the evidence if it detects the signs of domestic violence. The microphone will act as a sensor for audio queues where an AI will be used to determine if the sounds that it picks up is associated with domestic violence. If it does, the camera will be turned on and record the whole situation. The camera will also be equipped with an AI that can detect objects that it sees as to provide additional evidence. All recordings will be stored in cloud storage and the device itself will only store the latest recordings if it does not have enough space.

<div align="right">(<a href="#top">back to top</a>)</div>



### Description

This repository holds the code for Angelica's Raspberry Pi hardware. The code is written in Python.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- GETTING STARTED -->
## Getting Started

To test out the application you will need a working Raspberry Pi 3 or newer device. The code is to be run from the Raspberry Pi.

### Prerequisites
- Raspberry pi device, with the latest version of Raspbian operating system installed.
- Working Raspberry Pi Camera (with 15 Pin Connector) and USB Mic (mono audio).
- Installed Python 3 in the Raspberry Pi.
- Installed picamera and pyaudio libraries.

### Installation

1. Clone the repo on your Raspberry Pi.
   ```sh
   git clone https://gitlab.com/dpiofog/kdrt-raspberrypi.git
   ```
2. Go to the kdrt-raspberrypi directory and run postFootage.py
   ```sh
   python3 postFootage.py
   ```
   This will begin listening for audio and checking with sound ai to check for positive kdrt events.
   When a positive kdrt event is detected, recording of footage will be done and sent to the server.

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- USAGE EXAMPLES -->
## Usage

The purpose of this code is to record video evidence of kdrt events and send it to your mobile application [Angelica's Mobile App](https://gitlab.com/dpiofog/kdrt-frontend). New videos are recorded whenever it detects signs of domestic violence or abuse, and you can view the footage using this app.


<!-- MEMBERS -->
## Members

- [Gardyan Akbar](https://gitlab.com/giantsweetroll)
- [Eric Edgari](https://gitlab.com/Trutina1220)
- [Vincentius Gabriel](https://gitlab.com/kronmess)
- [Jocelyn Thiojaya](https://gitlab.com/jocelynthiojaya)

Project Link: [https://gitlab.com/dpiofog/](https://gitlab.com/dpiofog/)

<div align="right">(<a href="#top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

_Python libraries used in the making of the project_
* [picamera](https://picamera.readthedocs.io/en/release-1.13/)
* [PyAudio](https://pypi.org/project/PyAudio/)



<div align="right">(<a href="#top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
<!-- [contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://github.com/othneildrew/Best-README-Template/graphs/contributors
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues -->
<!-- [license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge -->
<!-- [license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/othneildrew -->
<!-- [product-screenshot]: images/screenshot.png -->
