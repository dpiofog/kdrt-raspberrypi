from recordAudio import *
from convertWavdata import *
import threading

import scipy
from scipy.io import wavfile
import scipy.signal

def threadAudio():
    threading.Timer(10.0, threadAudio).start()
    recordAudio(10)
    getWavdata("audiothread.wav")

threadAudio()
