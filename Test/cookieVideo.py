import picamera
from time import sleep
from subprocess import call

# Setup the camera
with picamera.PiCamera() as camera:
    # Start recording
    camera.start_recording("/home/pi/Desktop/cookie/cookie.h264")
    sleep(5)
    # Stop recording
    camera.stop_recording()

# The camera is now closed
print("Converting video")
# now we're doing linux commands in python wow
command = "MP4Box -add cookie.h264 convertedVideo.mp4" # REMEMBER TO CHANGE THIS 
# execute command
call([command], shell=True)
print("Video converted")