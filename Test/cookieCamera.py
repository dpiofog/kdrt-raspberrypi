import picamera

# Setup camera, closes when we are done
print("About to take a picture")
with picamera.PiCamera() as camera:
    camera.resolution = (1280, 720)
    camera.capture("/home/pi/Desktop/cookie/cookie.jpg")

print("Picture taken")