import subprocess
import os
from datetime import datetime

# combining video and audio
cmd = 'ffmpeg -y -i audio.wav  -r 30 -i video.h264  -filter:a aresample=async=1 -c:a flac -c:v copy footage.mkv'
subprocess.call(cmd, shell=True) # "Muxing Done
print('Muxing Done')

# renaming final video
device_name = "Device101"
dt = str(datetime.now())

old_name = "footage.mkv"
new_name = device_name + " " + dt + ".mkv"

if os.path.isfile(new_name):
    print("The file already exists")
else:
    # Rename the file
    os.rename(old_name, new_name)
    print("Rename successful")
