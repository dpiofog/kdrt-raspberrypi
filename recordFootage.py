import picamera
import pyaudio

import wave
from time import sleep
import subprocess
from subprocess import call
import os
from datetime import datetime

def recordFootage(duration):
    # Setup the camera
    with picamera.PiCamera() as camera:
        # CAMERA RECORDING
        camera.start_recording("/home/pi/Desktop/kdrt-camera/video.h264")
        print("camera recording")

        # AUDIO RECORDING
        form_1 = pyaudio.paInt16 # 16-bit resolution
        chans = 1 # 1 channel
        samp_rate = 44100 # 44.1kHz sampling rate
        chunk = 4096 # 2^12 samples for buffer
        record_secs = duration # recording for 5 seconds
        dev_index = 1 # device index found by p.get_device_info_by_index(ii)
        wav_output_filename = 'audio.wav' # name of .wav file
        audio = pyaudio.PyAudio() # create pyaudio instantiation

        # create pyaudio stream
        stream = audio.open(format = form_1,rate = samp_rate,channels = chans, \
                            input_device_index = dev_index,input = True, \
                            frames_per_buffer=chunk)
        print("audio recording")
        frames = []

        # SLEEP FOR CAMERA
        sleep(duration)

        # CAMERA STOP RECORDING 
        camera.stop_recording()
        print("camera finished recording")

        # AUDIO STOP RECCORDING
        # loop through stream and append audio chunks to frame array
        for ii in range(0,int((samp_rate/chunk)*record_secs)):
            data = stream.read(chunk, exception_on_overflow = False)
            frames.append(data)
        print("audio finished recording")

        # stop the stream, close it, and terminate the pyaudio instantiation
        stream.stop_stream()
        stream.close()
        audio.terminate()

        # save the audio frames as .wav file
        wavefile = wave.open(wav_output_filename,'wb')
        wavefile.setnchannels(chans)
        wavefile.setsampwidth(audio.get_sample_size(form_1))
        wavefile.setframerate(samp_rate)
        wavefile.writeframes(b''.join(frames))
        wavefile.close()
        print("recording saved")
        
        # combining video and audio
        cmd = 'ffmpeg -y -i audio.wav  -r 30 -i video.h264  -filter:a aresample=async=1 -c:a flac -c:v copy footage.mkv'
        subprocess.call(cmd, shell=True) # "Muxing Done
        print('Muxing Done')

recordFootage()
