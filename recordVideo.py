import picamera
from time import sleep
from subprocess import call

# Setup the camera
with picamera.PiCamera() as camera:
    # Start recording
    camera.start_recording("/home/pi/Desktop/kdrt-camera/video.h264")
    sleep(5)
    # Stop recording
    camera.stop_recording()
