import requests
import os
import subprocess
from datetime import datetime

from recordAudio import *
from generateName import *
from recordFootage import *
from time import sleep

#from scipy.io import wavfile
#import scipy.signal

deviceID = "devicesidtest"

audio_filename = "audiothread.wav"
#audio_filename = "keywordtest.wav"

#REVERSE_PROXY_SITE_NAME = "http://reverse-proxy-load-balancer-1196714148.ap-southeast-1.elb.amazonaws.com/api-detection"
REVERSE_PROXY_SITE_NAME = "http://reverse-proxy-load-balancer-1619605616.ap-southeast-1.elb.amazonaws.com/api-detection"
#url = "http://sound-ai-load-balancer-146746672.ap-southeast-1.elb.amazonaws.com/audio/predict"

listening = True


try:
    while listening:
        # Record 10s Audio
        # FUNCTION record audio
        recordAudio(10)
        #sleep(10)
        audio_file = open(audio_filename, "rb")

        # POST audio file 
        resp = requests.post(f'{REVERSE_PROXY_SITE_NAME}/audio/predict',files={"file" : audio_file} )
        #resp = requests.post(f'{url}',files={"file" : audio_file} )
        #print(f'Content:{resp.content}')

        # close and remove file
        audio_file.close()
        os.remove(audio_filename)

        if resp.ok:
            print(resp.json())
            if resp.json()["is_kdrt"] == True:
                # FUNCTION record video
                recordFootage(10)
                
                # rename video file and store it
                old_name = "footage.mkv"
                video_filename = generateName(deviceID)
                
                os.rename(old_name, video_filename)

                video_file = open(video_filename, "rb")

                try:
                    # POST video file
                    requests.post(f'{REVERSE_PROXY_SITE_NAME}/video/predict/{deviceID}',files={"file" : video_file}, timeout=2 ) 
                    #print(resp_vid.json())
                except requests.exceptions.ReadTimeout:
                    pass
                except Exception as e:
                    print(e)
                    #pass
                
                print("posted video")
                
                # close and remove file
                video_file.close()
                os.remove(video_filename)
                
        else: print(resp.status_code)

        

except requests.exceptions.RequestException as e:
    print(e)


