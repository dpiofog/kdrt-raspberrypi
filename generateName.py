import subprocess
import os
from datetime import datetime

def generateName(deviceName):
    # renaming final video
    device_name = deviceName
    dt = str(datetime.now()).replace(':','_')
    dt = dt.replace(' ','_')

    old_name = "footage.mkv"
    new_name = device_name + "_" + dt + ".mkv"
    
    return new_name

